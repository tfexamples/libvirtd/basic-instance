variable "os_image" {
  type    = string
  default = "https://repo.almalinux.org/almalinux/9/cloud/x86_64/images/AlmaLinux-9-GenericCloud-latest.x86_64.qcow2"
}

variable "ssh_key" {
  type    = string
  default = "terraform-libvirt.pub"
}

variable "instance" {
  type    = number
  default = 1
}

variable "instance_vcpu" {
  type    = number
  default = 1
}
variable "instance_memory" {
  type    = number
  default = 2048
}

variable "disk_size" {
  type    = number
  default = 85899345920
}
