terraform {
  required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "~> 0.6.14"
    }
  }
}

#  # Libvirt system
# provider "libvirt" {
#   uri = "qemu+ssh://user@ip/system"
# }

# Libvirt system
provider "libvirt" {
  uri = "qemu:///system"
}
